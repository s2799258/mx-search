console.log("Loaded");

$(document).ready(function(){
    console.log("Ready");
    
    $("#formInput").keyup(function(){
      var inputValue = $("#formInput").val();
      callAjax(inputValue);
    });

});

function callAjax(inputValue){
  
  var url = "http://mx-nav-stage.ap-southeast-2.elasticbeanstalk.com/api/search?q="+inputValue+"&type=webstore";
  
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": url,
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "a1f73116-dc91-29b3-6c95-484f8cdd8539"
    }
  };
    
    $.ajax(settings).done(function (response) {
      console.log(response);
      console.log("Categories length: " + response.data.categories.length);
      console.log("Products length: " + response.data.products.length);
      
      $("#list").html("");
      for(var i = 0; i < response.data.categories.length; i++){
        $("#list").append("<li>"+response.data.categories[i].name+"</li>");
      }
      
      $("#products").html("");
      for(var x = 0; x < response.data.products.length; x++){
        var imgSrc = response.data.products[x].image_url;
        var name = response.data.products[x].name;
        var price = response.data.products[x].price;
        $("#products").append("<div class='imgBox'><img class='productImg' src='" + imgSrc + "'><p class='title'>" + name + "</p><p>$" + price + "</p></div>");
      }
    });

}