<!DOCTYPE html>
<html>
    <head>
        <title>MX Search</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div id="main">
            <img id="MXimg" src="img/mxstore.png" alt="MX Store">
            <form>
                <input id="formInput" type="text" placeholder="Search">
            </form>
            <ul id="list">
            </ul>
            <div id="products">
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.0.js" integrity="sha256-wPFJNIFlVY49B+CuAIrDr932XSb6Jk3J1M22M3E2ylQ=" crossorigin="anonymous"></script>
        <script src="js/javascript.js"></script>
    </body>
</html>